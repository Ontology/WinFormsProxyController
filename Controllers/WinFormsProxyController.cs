﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.WebSocketServices;
using SecurityModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace WinFormsProxyController.Controllers
{
    public class WinFormsProxyController : WinFormsProxyViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private string userName;
        private string password;
        private string group;

        private Globals globals;

        private SecurityController securityController;


        private clsOntologyItem oItemUser;
        private clsOntologyItem oItemGroup;

        public OntoMsg_Module.StateMachines.IControllerStateMachine StateMachine { get; private set; }
        public OntoMsg_Module.StateMachines.ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (OntoMsg_Module.StateMachines.ControllerStateMachine)StateMachine;
            }
        }

        public WinFormsProxyController()
        {
            // local configuration
            globals = new Globals();

            Initialize();
        }

        private void Initialize()
        {
            var stateMachine = new OntoMsg_Module.StateMachines.ControllerStateMachine(OntoMsg_Module.StateMachines.StateMachineType.BlockSelectingSelected | OntoMsg_Module.StateMachines.StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            securityController = new SecurityController(globals);
            PropertyChanged += OutlookConnectorController_PropertyChanged;
        }

        private void StateMachine_closedSocket()
        {
            
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
        }

        private void StateMachine_loginSucceded()
        {
            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
         
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
        }

        private void OutlookConnectorController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            webSocketServiceAgent.SendPropertyChange(e.PropertyName);
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;


        }

      

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {



            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {
                if (webSocketServiceAgent.ChangedProperty.Key.ToLower() == "username")
                {
                    userName = webSocketServiceAgent.ChangedProperty.Value.ToString();
                    CheckLogin();
                }
                if (webSocketServiceAgent.ChangedProperty.Key.ToLower() == "password")
                {
                    password = webSocketServiceAgent.ChangedProperty.Value.ToString();
                    CheckLogin();
                }
                if (webSocketServiceAgent.ChangedProperty.Key.ToLower() == "group")
                {
                    group = webSocketServiceAgent.ChangedProperty.Value.ToString();
                    CheckLogin();
                }

            }
            else if (e.PropertyName == NotifyChanges.Websocket_ChangedViewItems)
            {
                if (!IsSuccessful_Login) return;
                webSocketServiceAgent.ChangedViewItems.ForEach(changedItem =>
                {
                    if (changedItem.ViewItemId == "OItem")
                    {
                        var oItem = Newtonsoft.Json.JsonConvert.DeserializeObject<clsOntologyItem>(changedItem.ViewItemValue.ToString());
                        if (oItem.Type == globals.Type_Class)
                        {
                            var interComMessage = new InterServiceMessage
                            {
                                UserId = oItemUser.GUID,
                                ChannelId = Channels.SelectedClassNode,
                                OItems = new List<clsOntologyItem>
                                {
                                    oItem
                                }
                            };
                            webSocketServiceAgent.SendInterModMessage(interComMessage);
                        }
                        else if (oItem.Type == globals.Type_Object)
                        {
                            var interComMessage = new InterServiceMessage
                            {
                                UserId = oItemUser.GUID,
                                ChannelId = Channels.ParameterList,
                                OItems = new List<clsOntologyItem>
                                {
                                    oItem
                                }
                            };
                            webSocketServiceAgent.SendInterModMessage(interComMessage);
                        }
                    }
                });
            }


        }

        private void CheckLogin()
        {
            if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(group) && !string.IsNullOrEmpty(password))
            {
                oItemUser = securityController.GetUser(userName);
                oItemGroup = securityController.GetGroup(group);


                if (oItemUser == null || oItemGroup == null)
                {

                    IsSuccessful_Login = false;
                }

                var result = securityController.InitializeUser(oItemUser, password);
                if (result.GUID == globals.LState_Success.GUID)
                {
                    IsSuccessful_Login = true;
                    webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
                    {
                        EndpointType = EndpointType.Sender
                    });
                }
                else
                {
                    IsSuccessful_Login = false;
                }
            }
        }

        private void WebSocketServiceAgent_comServerOpened()
        {


        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;

        }

        
        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
