﻿using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormsProxyController.Controllers
{
    public class WinFormsProxyViewModel : ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true)]
        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {

                issuccessful_Login = value;

                RaisePropertyChanged(nameof(IsSuccessful_Login));

            }
        }
    }
}
